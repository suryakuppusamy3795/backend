var baseCtrl = require("../controller/testCtrl")
var heroList = require("../helper/superHeroList")
module.exports = {
    testMiddleWare: (req, res, next) => {
        console.log("works fine")
        console.log(req.body, "requested body")
        var body = req.body
        var requestedText = Array.from(body.toString()).map(Number);
        req["combinatedArr"] = []
        var combinedText=baseCtrl.removeElement(requestedText,0)
        req.combinatedArr = baseCtrl.wordMapping(combinedText)
        next();

        
    },
    mapSuperHero: (req, res, next) => {
        var heroArr = heroList.heroArray
        var superHeroName = req.combinatedArr.find(val => {
            return heroArr.includes(val)
        })

        res.send({ "Name": superHeroName })
    }
}