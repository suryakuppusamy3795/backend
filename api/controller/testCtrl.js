module.exports = {
    wordMapping: (combinedArr) => {
        var keypad_map = [
            [],
            ['!', '@', '#'],
            ['A', 'B', 'C'],
            ['D', 'E', 'F'],
            ['G', 'H', 'I'],
            ['J', 'K', 'L'],
            ['M', 'N', 'O'],
            ['P', 'Q', 'R', 'S'],
            ['T', 'U', 'V'],
            ['W', 'X', 'Y', 'Z'],
          ];
        var letters = []
        var limit = 0
        var progress = 0
        var currentWord = ""
        var foundWords = ""
        for (var i = 0; i < combinedArr.length; i++) {
            letters.push(keypad_map[combinedArr[i]]);
        }
        limit = combinedArr.length;
        foundWords = [];
        var return_value = generateCombination(letters,progress,currentWord,limit,foundWords);
        console.log(return_value);
        return (return_value)


        function generateCombination(letters, progress, currentWord, limit, found_words){
            if (progress == limit) {
                found_words.push(currentWord);
              } else {
                for (var i = 0; i < letters[progress].length; i++) {
                  var next_word = currentWord + letters[progress][i];
          
                  generateCombination(letters,progress + 1,next_word,limit,found_words);
                }
              }
          
              return found_words;
        }
    },
    removeElement:(arrayName, arrayElement)=>{
      var i = arrayName.length;
      while (i--) {
          if (arrayName[i] === arrayElement) {
            arrayName.splice(i, 1);
          }
      }
      return arrayName;
  }
   
}