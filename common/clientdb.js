
var mongo = require("mongodb")
var mongoose = require("mongoose")
var MongoClient = mongo.MongoClient;
var newConnect={};
var dbConnection= function(dbName){
if(newConnect[dbName]){
    return newConnect[dbName]
}else{
    let options={
        server:{socketOptions:{keepAlive:1,connectTimeoutMS:300}},
        replset:{socketOptions:{keepAlive:1,connectTimeoutMS:300}},
    }
    newConnect[dbName] = mongoose.createConnection()
}
}
module.exports= dbConnection;