// module.export=()=>{
var express = require('express')
var libs = require('./common/lib')
const cors = require('cors');
const app = libs.express()
var request = require("request")
app.use(cors());
const port = 3000
var router = libs.express.Router();
const events = require('events')
const eventEmitter = new events.EventEmitter();
var mongo = require("mongodb")
var mongoose = require("mongoose")
var Schema = mongoose.Schema
var fs = require("fs");
const dburl = "mongodb://localhost/local" 
//  var connectdb=mongoose.connect("mongodb://localhost/local",{useNewUrlParser:true});
//  var db=mongoose.connection
//  .once('open',()=>console.log("connected Mongodb"))
//  .on("error",(error)=>{console.log(error,"mongo error")});
// app.use(function (req, res, next) {
//     req.header("Access-Control-Allow-Origin", "*");
//     req.header("Access-Control-Allow-Headers", "Origin, X-Requested-with,Content-Type,Accept");
//     next();
// });
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
// mongoose.model('startup_log',{name:String})
app.use(libs.bodyParser.json({ type: 'application/vnd.api+json' }))
// create application/json parser
app.use(libs.bodyParser.json());
// parse various different custom JSON types as JSON
app.use(libs.bodyParser.json({ type: 'application/*+json' }));
// parse some custom thing into a Buffer
app.use(libs.bodyParser.raw({ type: 'application/vnd.custom-type' }));
// parse an HTML body into a string
app.use(libs.bodyParser.text({ type: 'text/html' }));
// parse an text body into a string
app.use(libs.bodyParser.text({ type: 'text/plain' }));
// create application/x-www-form-urlencoded parser
app.use(libs.bodyParser.urlencoded({ extended: true }));
// fs.readdirSync(__dirname+'models').forEach(function(filename))
// var baseApiCall = require('../nodego/api/baseApi')
// var keyMidWare = require('./api/middleware/keyMap')
// const startup_log = require("../backend/models/ques")
app.get('/getData', (req, res) => {
    
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("local");
    dbo.collection("lang_list").find({},{_id:0}).toArray(function(err, result) {
      if (err) throw err;
    //   console.log(result,"result_dbbbbb");
    res.send(result)
      db.close();
    });
  });

})

app.get('/getQues/:lang', (req, res) => {
    
  MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("local");
      console.log(req.params.lang,"req.params.lang")
      var query={"script_lang":req.params.lang}
      dbo.collection("testCollection").find(query,{_id:0}).toArray(function(err, result) {
        if (err) throw err;
      //   console.log(result,"result_dbbbbb");
      res.send(result)
        db.close();
      });
    });
  
  })
// app.post('/superHeroSearch', keyMidWare.testMiddleWare,keyMidWare.mapSuperHero)

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
// }